#!/bin/bash

set -eux

# install ansible with pip3
sudo apt -y update
sudo apt install -y python3-pip
pip3 install ansible
sudo ln -s ~/.local/bin/ansible /usr/local/bin/ansible

# install git
sudo apt -y install git

sudo ln -s ~/.local/bin/ansible /usr/local/bin/ansible
sudo ln -s ~/.local/bin/ansible-playbook /usr/local/bin/ansible-playbook
sudo ln -s ~/.local/bin/ansible-galaxy /usr/local/bin/ansible-galaxy
sudo ln -s ~/.local/bin/ansible-playbook /usr/local/bin/ansible-playbook
sudo ln -s ~/.local/bin/ansible-galaxy /usr/local/bin/ansible-galaxy
sudo ln -s ~/.local/bin/ansible-vault /usr/local/bin/ansible-vault

# sleep patch for dell xps 13 9370
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash mem_sleep_default=deep"/g' /etc/default/grub
